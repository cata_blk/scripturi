# Niste script-uri mai utile sau mai putin utile

SELECT * FROM customers inner join orders on customers.id = orders.customer_id;


# cod pentru MySQL

create database recap77;

use recap77;

create table persoane(id int primary key auto_increment, nume varchar(240) not null, prenume varchar(240) not null, varsta int);

insert into persoane(nume, prenume, varsta) values('ionescu', 'ion', 20), ('ionescu', 'ionica', 20), ('ionescu', 'marius', 30),
							('vasilescu', 'vasile', 30), ('georgescu', 'vasile', 40);
                            
                            
select * from persoane;

create table cumparaturi(id int primary key auto_increment, numeprodus varchar(240) not null, pret double not null);
alter table cumparaturi add column id_persoana int;
alter table cumparaturi add constraint fk_pers foreign key(id_persoana) references persoane(id);

select * from cumparaturi;
select * from persoane;

insert into cumparaturi(numeprodus, pret, id_persoana) values('pizza', 10.0, 1), ('pasta', 20.0, 1), ('calzzone', 30.0, 2), ('cola', 4.0, 4);

select p.id, sum(c.pret) from persoane p inner join cumparaturi c on c.id_persoana = p.id  group by p.id;

select p.id, p.nume, p.prenume, sum(c.pret) from persoane p inner join cumparaturi c on c.id_persoana = p.id  group by p.id, p.nume, p.prenume;

select p.id, p.nume, p.prenume, c.SUMA from persoane p inner join (select cumparaturi.id_persoana IDPERS,  sum(cumparaturi.pret) SUMA from cumparaturi group by IDPERS) c on c.IDPERS = p.id;

select cumparaturi.id_persoana IDPERS,  sum(cumparaturi.pret) SUMA from cumparaturi group by IDPERS;

-- http://weblogs.sqlteam.com/jeffs/archive/2005/12/14/8546.aspx -> ca sa eviti group by multiplu